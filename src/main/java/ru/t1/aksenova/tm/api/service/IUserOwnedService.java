package ru.t1.aksenova.tm.api.service;

import ru.t1.aksenova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnerModel> extends IService<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    void removeAll(String userId);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M remove(String userId, M model);

    M removeOneById(String userId, String id);

    M removeOneByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

}
