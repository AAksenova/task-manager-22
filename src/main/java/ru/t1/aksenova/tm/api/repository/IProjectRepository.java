package ru.t1.aksenova.tm.api.repository;

import ru.t1.aksenova.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description);

}
