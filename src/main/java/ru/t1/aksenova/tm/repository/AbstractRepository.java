package ru.t1.aksenova.tm.repository;

import ru.t1.aksenova.tm.api.repository.IRepository;
import ru.t1.aksenova.tm.exception.field.IndexIncorrectException;
import ru.t1.aksenova.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> records = new ArrayList<>();

    @Override
    public M add(final M model) {
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return records;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(records);
        result.sort(comparator);
        return result;
    }

    @Override
    public void removeAll() {
        records.clear();
    }

    @Override
    public M findOneById(final String id) {
        return records
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        final int length = records.size();
        if (records.isEmpty() || length <= index) throw new IndexIncorrectException();
        return records
                .stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public M removeOne(final M model) {
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public M removeOneById(final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public M removeOneByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        final M model = findOneById(id);
        if (model == null) return false;
        return true;
    }

}
