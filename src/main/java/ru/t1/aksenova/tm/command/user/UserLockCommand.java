package ru.t1.aksenova.tm.command.user;

import ru.t1.aksenova.tm.api.service.IUserService;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    public static final String NAME = "user-lock";

    public static final String DESCRIPTION = "User lock.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final IUserService userService = serviceLocator.getUserService();
        userService.lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
