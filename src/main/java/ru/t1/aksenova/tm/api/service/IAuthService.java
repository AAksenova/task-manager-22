package ru.t1.aksenova.tm.api.service;

import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.model.User;

public interface IAuthService {

    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

    void checkRoles(Role[] roles);

}
