package ru.t1.aksenova.tm.command.project;

import ru.t1.aksenova.tm.enumerated.ProjectSort;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list";

    public static final String DESCRIPTION = "Show list projects.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        final String sortType = TerminalUtil.nextLine();

        ProjectSort sort = ProjectSort.toSort(sortType);
        if (sort == null) {
            sort = ProjectSort.toSort("BY_CREATED");
            System.out.println("DEFAULT SORT: BY_CREATED");
        }
        final List<Project> projects = getProjectService().findAll(userId, sort.getComparator());
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + ": " + project.getStatus());
            index++;
        }
    }

}
