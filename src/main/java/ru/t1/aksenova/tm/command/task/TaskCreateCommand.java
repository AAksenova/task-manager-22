package ru.t1.aksenova.tm.command.task;

import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    public static final String NAME = "task-create";

    public static final String DESCRIPTION = "Create new task.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        getTaskService().create(userId, name, description);
    }

}
