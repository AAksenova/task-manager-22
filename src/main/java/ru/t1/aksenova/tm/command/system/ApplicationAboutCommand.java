package ru.t1.aksenova.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String NAME = "about";

    public static final String DESCRIPTION = "Show about program.";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasiya Aksenova");
        System.out.println("e-mail: aaksenova@t1-consulting.ru");
        System.out.println("e-mail: cs.aksenova@gmail.com");
    }

}
