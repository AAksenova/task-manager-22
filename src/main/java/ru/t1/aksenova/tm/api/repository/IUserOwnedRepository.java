package ru.t1.aksenova.tm.api.repository;

import ru.t1.aksenova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    M removeOne(String userId, M model);

    M removeOneById(String userId, String id);

    M removeOneByIndex(String userId, Integer index);

    void removeAll(String userId);

}
