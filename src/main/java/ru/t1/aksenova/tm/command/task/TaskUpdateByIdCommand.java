package ru.t1.aksenova.tm.command.task;

import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-update-by-id";

    public static final String DESCRIPTION = "Update task by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();

        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        getTaskService().updateById(userId, id, name, description);
    }

}
